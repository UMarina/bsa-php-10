<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Jobs\CropJob;

class PhotoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|image',
        ]);

        $file = $request->file('photo');

        $user_id = auth()->id();
        $fileName = $file->getClientOriginalName();
        $directory = 'images/' . $user_id;
        $path_to_original = $file->storeAs($directory, $fileName);

        $photo = Photo::create([
            'user_id' => $user_id,
            'original_photo' => $path_to_original,
            'status' => 'uploaded'
        ]);

        dispatch((new CropJob($photo))->onQueue('photos'));

        return response()->json([
            'message' => 'Attachment has been successfully uploaded.',
        ]);
    }
}
