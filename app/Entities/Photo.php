<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';

    protected $fillable = [
        'user_id', 'original_photo', 'photo_100_100', 'photo_150_150', 'photo_250_250', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
