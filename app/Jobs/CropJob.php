<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\PhotoService;
use Illuminate\Support\Facades\Storage;



class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $photo;
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = $photo->user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhotoService $photoService)
    {

        $pathToPhoto = $this->photo->original_photo;

        $filename = pathinfo($pathToPhoto, PATHINFO_FILENAME);
        $dirname = pathinfo($pathToPhoto, PATHINFO_DIRNAME);
        $extension = pathinfo($pathToPhoto, PATHINFO_EXTENSION);

        $this->photo->update(['status' => 'processing']);

        $pathToPhoto100 = $photoService->crop($pathToPhoto, 100, 100);
        $pathToMove100 = $dirname . '/' . $filename . '100x100.' . $extension;

        Storage::move($pathToPhoto100, $pathToMove100 );

        $this->photo->update(['photo_100_100' => $pathToMove100]);

        $pathToPhoto150 = $photoService->crop($pathToPhoto, 150, 150);
        $pathToMove150 = $dirname . '/' . $filename . '150x150.' . $extension;

        Storage::move($pathToPhoto150, $pathToMove150 );

        $this->photo->update(['photo_150_150' => $pathToMove150]);

        $pathToPhoto250 = $photoService->crop($pathToPhoto, 250, 250);
        $pathToMove250 = $dirname . '/' . $filename . '250x250.' . $extension;

        Storage::move($pathToPhoto250, $pathToMove250 );

        $this->photo->update([
            'photo_250_250' => $pathToMove250,
            'status' => 'success'
        ]);

        $this->user->notify(new ImageProcessedNotification($this->photo));
    }

    public function failed()
    {
        $this->photo->update([
            'status' => 'fail'
        ]);

        $this->user->notify(new ImageProcessingFailedNotification($this->photo));
    }
}
